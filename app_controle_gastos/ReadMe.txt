No windows
Ter instalado o Python
Instalar a Ide - Pycharm no caso verificar possibilidade de utilizar o vscode

Criar uma virtual env
python -m venv venv
cd venv
cd Scripts
activate

pip install django

No Linux

no terminal
python3

caso não tenha instalada 
sudo apt-get install python3.6

mkdir projeto

cd projeto
python3 -m venv myvenv
ls
caso não funcione sudo apt-get install python3.6-venv
source myvenv/bin/activate
pip install django

------------
python3
import django
django.VERSION

------------

Pycharm

Criar uma pasta
Criar uma venv
activate a venv
pip install django dentro da venv
criar projeto django

django-admin startproject "NOME DO Projeto"
OU
django-admin startproject "NOME DO Projeto" . - Não cria uma pasta com subpasta para o projeto

Estrutura do Django
_init_.py - Pacote Python
settings.py - Setup do projeto - BaseDir, Secret Key (Criptografar dados do sistema), Debug, middleware, database
urls.py - Urls do projeto
wsgi.py - Entrypoint quando publicada na Web
Manage.py - Utilitario, criar usuário, migrates, etc.

------------

Criando aplicação de controle de gastos
Descrição da despesa
Valor

python manage.py startapp contas
app do django
ir no settings.py do controle_gastos e registrar a app

INSTALLED_APPS = [
    ...
    'contas',
]

criar o banco de dados
python manage.py migrate
Cria o banco de dados

Executa a aplicação de forma local
python manage.py runserver

python manage.py createsuperuser
Cria um super usuário para logar na aplicação django (App admin do django)

Executa a aplicação de forma local
python manage.py runserver

Para entrar no admin da aplicação - Com o usuário e senha que foram criados
http://127.0.0.1:8000/admin/

-----
Para acessar as views elas devem estar sendo listadas na urls.py
Só assim o django permite acessar elas,
Exemplo
from contas.views import home
urlpatterns = [
    ...
    path('contas', home)
]
https://docs.djangoproject.com/en/3.0/topics/http/urls/
-----
Criação da primeira view
https://docs.djangoproject.com/en/3.0/topics/http/views/
-----
Retornar um template
Criar uma pasta template
na views.py onde a função retornara o template a ser utilizado
-----
Models

Classe em python que referencia a tabela do banco de dados

Assim que fizer a classse com as respectivas fields
deverá no console realizar o makemigrations e migrate

pyhton manage.py makemigrations
python manage.py migrate

No admin.py
Adicionar o model Categoria para realizar a manutenção dos registros no banco pelo django admin
from .models import Categoria

admin.site.register(Categoria)
-----
Classe de transação
-----
CRUD



